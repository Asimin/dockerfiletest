FROM nginx

# Create app directory
RUN mkdir -p /usr/share/nginx/html/

# Change working dir to /usr/share/nginx/html
WORKDIR ./html/* /usr/share/nginx/html/

# Add volumes for nginx
VOLUME  ["/usr/share/nginx/html/", "/var/lib/docker" ]

EXPOSE 8080
